﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ???
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Input;
using System.Windows;

// SEMEN
using Mapsui.Projection;
using WpfMapControl;
using Mapsui.Geometries;
using Mapsui.Styles;


namespace MapRastr
{

    // -----------------------------------------------------------------------------------
    // Структура вместо стандартной Point

    public struct MyPoint
    {
        public int X;
        public int Y;
    };

    // ------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------
    // Для ЗПВ

    [Serializable]
    public struct KoordThree1
    {
        public KoordThree1(double x, double y, double h)
        {
            this.x = x;
            this.y = y;
            this.h = h;
        }

        public double x;
        public double y;
        public double h;

        public static bool operator ==(KoordThree1 a, KoordThree1 b)
        {
            return Math.Abs(a.x - b.x) < 1e-5
                   && Math.Abs(a.y - b.y) < 1e-5
                   && Math.Abs(a.h - b.h) < 1e-5;
        }

        public static bool operator !=(KoordThree1 a, KoordThree1 b)
        {
            return !(a == b);
        }
    }
    public struct KoordTwo1
    {
        public double x;
        public double y;
    }
    // ---------------------------------------------------------------------


    public class ClassMapRastr
    {
        // Расчет средней высоты местности ************************************************
        // Функция определения средней высота местности

        // !!! Point из Mapsui.Geometries (double,double)
        //private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        public int DefineMiddleHeight_Comm(
                                            MyPoint tpReferencePoint, // Центр зоны
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl,
                                            int iRadius,
                                            int iStep
                                            )
        {

            // Otladka
            iRadius = 30000;
            iStep = 100;

            int iCount = 0;
            double dMiddleHeightStep = 0;
            int iMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                int iMinX = 0;
                int iMinY = 0;
                int iMaxX = 0;
                int iMaxY = 0;

                iMinX = (int)(tpReferencePoint.X - iRadius);
                iMinY = (int)(tpReferencePoint.Y - iRadius);
                iMaxX = (int)(tpReferencePoint.X + iRadius);
                iMaxY = (int)(tpReferencePoint.Y + iRadius);

                // пройти по координатам карты с шагом Shag
                for (int i = iMinX; i < iMaxX; i = i + iStep)
                {
                    for (int j = iMinY; j < iMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;
                        dSetX = i;
                        dSetY = j;

                        var p5 = Mercator.ToLonLat(dSetX, dSetY);
                        var lat = p5.Y;
                        var lon = p5.X;

                        try
                        {
                            var x = objRasterMapControl.DtedMass.GetElevation(lon, lat);

                            if (x == null)
                                dMiddleHeightStep = 0;
                            else if (x < 0)
                                dMiddleHeightStep = 0;
                            else
                                dMiddleHeightStep = (double)x;
                        }
                        catch
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        iMiddleHeight = (int)((double)iMiddleHeight + dMiddleHeightStep);

                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                iMiddleHeight = (int)((double)iMiddleHeight / (double)iCount);

                if (iMiddleHeight < 0)
                    iMiddleHeight = 0;

            } // IF

            return iMiddleHeight;

        }
        // ************************************************ Расчет средней высоты местности

        // Расчет точек ДПВ ***************************************************************
        public int CountDSR(
                             //int iHeightTotalOwn,    // Hantsp+Hrelsp
                             int iHeightOwnAnten,    // Hantsp
                             int iHeightOwnObj,      // Hrelsp
                             int iHeightMiddle,      // Hsredn
                             //int iHeightOpponentObj  // Hsredn+Hantop
                             int iHeightOpponentAnten  // Hantop
                            )
        {
            int iDSR = 0;
            int h1 = 0;
            int h2 = 0;
            int iHeightTotalOwn = 0;     // Hantsp+Hrelsp
            int iHeightOpponentObj = 0;  // Hsredn+Hantop

            // -------------------------------------------------------------

            // Hantsp+Hrelsp
            iHeightTotalOwn = iHeightOwnAnten + iHeightOwnObj;

            // Hsredn+Hantop
            iHeightOpponentObj = iHeightMiddle + iHeightOpponentAnten;
            // -------------------------------------------------------------

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // ***
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hantop

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hantop

            } // iHeightMiddle > iHeightOwnObj

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // ДЛЯ КАРТИНОК *****************************************************
            // FOR sea_eatrh
            /*
                        // sea
                        h1 = iHeightTotalOwn;                    // Hantcp+Hrelcp
                        h2 = iHeightOpponentAnten;               // это Hantop

                        //eatrh
                        h1 = iHeightTotalOwn - iHeightMiddle;    // Hantcp+Hrelcp-Hsr
                        h2 = iHeightOpponentAnten; // Hantop
            */
            // ***************************************************** ДЛЯ КАРТИНОК

            // рассчитать ДПВ по формуле
            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            iDSR = iDSR * 1000;

            return iDSR;
        }
        // *************************************************************** Расчет точек ДПВ

        // Расчет ЗПВ *********************************************************************
        // 27_09_2018

        //public List<Point> CreateLineSightPolygon(
        public List<MyPoint> CreateLineSightPolygon(

                                                 // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                 RasterMapControl objRasterMapControl,

                                                 //Point tpCenterLSR,
                                                 MyPoint tpCenterLSR,  // Центр зоны

                                                 int iHeightCenterLSR, // HrelSP+Hantsp
                                                 double dDSR,
                                                 int iHeightAntenOpponent, // iOpponAnten_comm
                                                 // шаг изменения угла в град
                                                 int iStepAngleInput_ZPV,
                                                 // шаг в м
                                                 int iStepLengthInput_ZPV
                                                 ) 
        {
            // --------------------------------------------------------------------------------------
            //var listKoordReal = new List<Point>();
            var listKoordReal = new List<MyPoint>();
            MyPoint objMyPoint = new MyPoint();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;
            double latpp = 0;
            double longpp = 0;
            int RADIUS_EARTH = 8500000;

            // otl
            iStepAngleInput_ZPV = 1; //grad
            iStepLengthInput_ZPV = 100; //m
            // --------------------------------------------------------------------------------------

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));
            // --------------------------------------------------------------------------------------

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + iStepAngleInput_ZPV)
            //for (angleFi = 360; angleFi > 0; angleFi = angleFi - iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));
                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree1 koord2;
                //0210
                koord2.x = (double)tpCenterLSR.X + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.y = (double)tpCenterLSR.Y + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                //0213
                var ppp = Mercator.ToLonLat(dSetX, dSetY);
                latpp = ppp.Y;
                longpp = ppp.X;
                try
                {
                    var xpp = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                    if (xpp == null)
                        koord2.h = 0;
                    else if (xpp < 0)
                        koord2.h = 0;
                    else
                        koord2.h = (double)xpp;
                }
                catch
                {
                    koord2.h = 0;
                }

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны
                // координатам точки реальной ДПВ
                KoordThree1 koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................
                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ

                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                KoordThree1 koord3;
                KoordThree1 koordPrev;

                // 27_09_2018
                KoordThree1 koordPrev1;

                // IF1
                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    //0210
                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.x = tpCenterLSR.X + iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.y = tpCenterLSR.Y + iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    //0213
                    var ppp1 = Mercator.ToLonLat(dSetX, dSetY);
                    latpp = ppp1.Y;
                    longpp = ppp1.X;
                    try
                    {
                        var xpp1 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                        if (xpp1 == null)
                            koord3.h = 0;
                        else if (xpp1 < 0)
                            koord3.h = 0;
                        else
                            koord3.h = (double)xpp1;
                    }
                    catch
                    {
                        koord3.h = 0;
                    }

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                //0210
                                // расссчитать координаты в этой точке
                                koord3.x = tpCenterLSR.X + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.y = tpCenterLSR.Y + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;

                                //0213
                                var ppp2 = Mercator.ToLonLat(dSetX, dSetY);
                                latpp = ppp2.Y;
                                longpp = ppp2.X;
                                try
                                {
                                    var xpp2 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                                    if (xpp2 == null)
                                        koord3.h = 0;
                                    else if (xpp2 < 0)
                                        koord3.h = 0;
                                    else
                                        koord3.h = (double)xpp2;
                                }
                                catch
                                {
                                    koord3.h = 0;
                                }

                                h_Dob = dL * (dDSR - dL) / (2 * RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");

                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }

                                    else // Еще видим ОП
                                    {

                                        // otl*** !!! При отладочной записи в файл НЕ надо откомментировать
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + iStepLengthInput_ZPV;
                                        // otl***
                                        //srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + iStepLengthInput_ZPV;

                            //0210
                            // расссчитать координаты в этой точке
                            koord3.x = tpCenterLSR.X + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.y = tpCenterLSR.Y + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            //0213
                            var ppp3 = Mercator.ToLonLat(dSetX, dSetY);
                            latpp = ppp3.Y;
                            longpp = ppp3.X;
                            try
                            {
                                var xpp3 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                                if (xpp3 == null)
                                    koord3.h = 0;
                                else if (xpp3 < 0)
                                    koord3.h = 0;
                                else
                                    koord3.h = (double)xpp3;
                            }
                            catch
                            {
                                koord3.h = 0;
                            }

                            // otl***
                            //srFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");                    

                    objMyPoint.X = (int)koord4.x;
                    objMyPoint.Y = (int)koord4.y;
                    //listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));
                    listKoordReal.Add(objMyPoint);


                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //srFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    //0210
                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.x = tpCenterLSR.X + iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.y = tpCenterLSR.Y + iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);

                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    //0213
                    var ppp4 = Mercator.ToLonLat(dSetX, dSetY);
                    latpp = ppp4.Y;
                    longpp = ppp4.X;
                    try
                    {
                        var xpp4 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                        if (xpp4 == null)
                            koord3.h = 0;
                        else if (xpp4 < 0)
                            koord3.h = 0;
                        else
                            koord3.h = (double)xpp4;
                    }
                    catch
                    {
                        koord3.h = 0;
                    }

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {

                        //??????????????????????????????? ***
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;

                                // высота воображаемой линии в точки с координатами Koord3
                                //0210
                                // расссчитать координаты в этой точке
                                koord3.x = tpCenterLSR.X + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.y = tpCenterLSR.Y + dL * Math.Sin((angleFi * Math.PI) / 180);

                                dSetX = koord3.x;
                                dSetY = koord3.y;

                                //0213
                                var ppp5 = Mercator.ToLonLat(dSetX, dSetY);
                                latpp = ppp5.Y;
                                longpp = ppp5.X;
                                try
                                {
                                    var xpp5 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                                    if (xpp5 == null)
                                        koord3.h = 0;
                                    else if (xpp5 < 0)
                                        koord3.h = 0;
                                    else
                                        koord3.h = (double)xpp5;
                                }
                                catch
                                {
                                    koord3.h = 0;
                                }

                                h_Dob = dL * (dDSR - dL) / (2 * RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    // &&&
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");

                                        // выйти из цикла while4
                                        dL = dDSR + 1;
                                    }

                                    else // Еще видим ОП
                                    {
                                        // &&&
                                        //koordPrev = koord3; // &&&
                                        dL = dL + iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + iStepLengthInput_ZPV;

                            //0210
                            // расссчитать координаты в этой точке
                            koord3.x = tpCenterLSR.X + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.y = tpCenterLSR.Y + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            //0213
                            var ppp6 = Mercator.ToLonLat(dSetX, dSetY);
                            latpp = ppp6.Y;
                            longpp = ppp6.X;
                            try
                            {
                                var xpp6 = objRasterMapControl.DtedMass.GetElevation(longpp, latpp);
                                if (xpp6 == null)
                                    koord3.h = 0;
                                else if (xpp6 < 0)
                                    koord3.h = 0;
                                else
                                    koord3.h = (double)xpp6;
                            }
                            catch
                            {
                                koord3.h = 0;
                            }

                            // otl***
                            //srFile.WriteLine("(12)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // zz
                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    objMyPoint.X = (int)koord4.x;
                    objMyPoint.Y = (int)koord4.y;
                    //listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));
                    listKoordReal.Add(objMyPoint);


                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
              // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //0210
            /*
                        for (int lk = 0; lk < listKoordReal.Count; lk++)
                        {
                            Point[] mass1 = listKoordReal.ToArray();
                            var xp=mass1[lk].Y;
                            var yp=mass1[lk].X;
                            mass1[lk].X=xp;
                            mass1[lk].Y=yp;
                            listKoordReal=mass1.ToList();
                        }
            */
            //otl***
            //srFile.Close();

            return listKoordReal; // List MyPoints

        } // P/P  Расчет ЗПВ (CreateLineSightPolygon)
          // ********************************************************************* Расчет ЗПВ

        // Пересчет рассчитанных точек ЗПВ **************************************************
        // Пересчет рассчитанных точек ЗПВ в координаты в системе координат с центром в центре
        // зоны ЗПВ (чтобы нарисовать пологон в окружности)

        //public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        public List<MyPoint> CreateLineSightPolygon_Circle(
                                                          // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                          RasterMapControl objRasterMapControl,

                                                          MyPoint tpCenterLSR,
                                                          int iHeightCenterLSR, // HrelSP+Hantsp

                                                          int iHeightOwnAnten,    // Hantsp
                                                          int iHeightOwnObj,      // Hrelsp
                                                          int iHeightMiddle,      // Hsredn
                                                          int iHeightOpponentAnten,  // Hantop

                                                          // шаг изменения угла в град при расчете ЗПВ
                                                          int iStepAngleInput_ZPV,
                                                          // шаг в м при расчете ЗПВ
                                                          int iStepLengthInput_ZPV,

                                                          int maxRadius = int.MaxValue
                                                          )

        {
            int idsr = 0;
            double dsr = 0;

            //var dsr = CountDSR(tpCenterLSR);
            idsr = CountDSR(
                            iHeightOwnAnten,    // Hantsp
                            iHeightOwnObj,      // Hrelsp
                            iHeightMiddle,      // Hsredn
                            iHeightOpponentAnten  // Hantop
                           );
            dsr = (double)idsr;

            // ***
            //var heightAntennOwnComm = HeightAntennOwn_comm;
            //var heightTotalOwnComm = HeightTotalOwn_comm;
            //var iOpponAntenComm = iOpponAnten_comm;

            //var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, iOpponAntenComm);

            // points-> List<MyPoints>
            var points = CreateLineSightPolygon(
                                                 // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                 objRasterMapControl,

                                                 tpCenterLSR,  // Центр зоны

                                                 iHeightCenterLSR, // HrelSP+Hantsp
                                                 dsr,
                                                 iHeightOpponentAnten, // Hantenop
                                                 // шаг изменения угла в град
                                                 iStepAngleInput_ZPV,
                                                 // шаг в м
                                                 iStepLengthInput_ZPV
                                               );

            MyPoint objMyPoint = new MyPoint();

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = (double)p.X - (double)tpCenterLSR.X;
                double dy = (double)p.Y - (double)tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                //var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                //var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;
                var x = tpCenterLSR.X + Math.Cos(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Sin(i * Math.PI / 180) * distance;

                //points[i] = new Point((int)x, (int)y);

                objMyPoint.X = (int)x;
                objMyPoint.Y = (int)y;
                points[i] = objMyPoint;

            }



            return points;
        }
        // --------------------------------------------------------------------------

        // ************************************************** Пересчет рассчитанных точек ЗПВ

        // Зона обнаружения СП **************************************************************

        public List<MyPoint> DetectionSP_Zone(
                                              // Частота передатчика связи,Hz
                                              double f,
                                              // Мощность передатчика связи, Wt
                                              double p,
                                              // К-т усиления пеленгатора
                                              double kpel,
                                              // Порог чувствительности пеленгатора,dBW
                                              // !!! <0
                                              int iKP,

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              RasterMapControl objRasterMapControl,

                                              MyPoint tpCenterLSR,
                                              int iHeightCenterLSR, // HrelSP+Hantsp

                                              int iHeightOwnAnten,    // Hantsp
                                              int iHeightOwnObj,      // Hrelsp
                                              int iHeightMiddle,      // Hsredn
                                              int iHeightOpponentAnten,  // Hantop

                                              // шаг изменения угла в град при расчете ЗПВ
                                              int iStepAngleInput_ZPV,
                                              // шаг в м при расчете ЗПВ
                                              int iStepLengthInput_ZPV,

                                              ref int RZone
                                              )

        {

            double dKP = 0;
            double ipw = 0;
            // ------------------------------------------------------------------------------
            double vc= 300000000; // Скорость света
            // ------------------------------------------------------------------------------
            // Порог чувствительности пеленгатора

            ipw = iKP / 10.0;
            dKP = Math.Pow(10, (double)ipw);
            // ------------------------------------------------------------------------------
            // Вычисление радиуса зоны

            var k = 1 + f / 2.4e9;
            if (f > 2.4e9)
            {
                k = 2;
            }

            RZone = (int)(k / 5 * (vc / (4 * Math.PI * f)) *Math.Sqrt((p * kpel) / dKP));
            // ------------------------------------------------------------------------------

            var pol = CreateLineSightPolygon_Circle(
                                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                  objRasterMapControl,

                                                  tpCenterLSR,
                                                  iHeightCenterLSR, // HrelSP+Hantsp

                                                  iHeightOwnAnten,    // Hantsp
                                                  iHeightOwnObj,      // Hrelsp
                                                  iHeightMiddle,      // Hsredn
                                                  iHeightOpponentAnten,  // Hantop

                                                  // шаг изменения угла в град при расчете ЗПВ
                                                  iStepAngleInput_ZPV,
                                                  // шаг в м при расчете ЗПВ
                                                  iStepLengthInput_ZPV,
                                                  RZone
                                                 );


            return pol;
        }
        // ************************************************************** Зона обнаружения СП

        // Distance *************************************************************************
        // Функция определения расстояния между объектами

        public double DefineDistanceObject(
                                           MyPoint tpPointOwn, 
                                           MyPoint tpPointOpponent
                                          )
        {
            double dDistObject = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;

            dDistObject = Math.Sqrt((x1 - x2) * (x1 - x2) +(y1 - y2) * (y1 - y2));

            return dDistObject;

        }
        // ************************************************************************* Distance

        // R_Zone_Suppression ***************************************************************
        // Радиус зоны подавления радиолиний управления

        public double Define_R_Zone_Suppression(
                                                // Hrelsp
                                                int iHeightCoordOwn, 
                                                // Hantsp
                                                int iHeightAntenOwn, 
                                                //int iHeightPlaneOpponent, 
                                                //Hantop_prm
                                                int iHeightAntenOpponRcv,
                                                // Hsredn
                                                int mdl
                                               )
        {
            // ------------------------------------------------------------------------------
            double dDistSup = 0;
            double l_p = 0;
            double l_pr = 0;
            int iHeightPlaneOpponent = 0;
            // ------------------------------------------------------------------------------
            iHeightPlaneOpponent = mdl + iHeightAntenOpponRcv;
            // ------------------------------------------------------------------------------
            double mdl1 = Math.Min(iHeightCoordOwn, mdl);
            // ------------------------------------------------------------------------------
            l_p = iHeightCoordOwn + iHeightAntenOwn - mdl1;
            l_pr = iHeightPlaneOpponent - mdl1;
            // ------------------------------------------------------------------------------
            dDistSup = 4120 * (Math.Sqrt(l_p) + Math.Sqrt(l_pr));
            // ------------------------------------------------------------------------------
            return dDistSup;
            // ------------------------------------------------------------------------------

        }
        // *************************************************************** R_Zone_Suppression

        // А_suppression  *******************************************************************
        // Функция определения коэффициента A для зоны подавления радиолиний управления

        public double DefineCoeffA(
                                   // Мощность,Вт,СП
                                   double iPowerOwn,
                                   // К-т усиления, СП
                                   double dCoeffOwn,
                                   // =1 пока подаем
                                   double iGamma,
                                   // Мощность,Вт,ОП
                                   double iPowerOpponent,
                                   // К-т усиления, ОП
                                   double dCoeffOpponent,
                                   // К-т подавления
                                   double dKp,
                                   // Hantsp
                                   double HASP,
                                   // Hantop
                                   double HAOP,
                                   // Hantop_prm
                                   double HAOP1
                                   )
        {
            double dCoeffA = 0;
            double A1 = 0;

            if ((HAOP > 50) || (HAOP1 > 50))
                dCoeffA = Math.Sqrt((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp));
            else
            {
                A1 = ((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp)) *
                    ((HASP * HASP) / (HAOP * HAOP));

                dCoeffA = Math.Pow(A1, 0.25);

            }
            return dCoeffA;

        }
        //  ******************************************************************* А_suppression

        // R_Zone_NoSuppression *************************************************************
        // Функция определения радиуса зоны неподавления (для зоны подавления радиолиний управления)

        public double DefineZoneNotSup(
                                      double dDistObj,
                                      //ref double dCoeffA
                                      double dCoeffA,
                                      // =2 -> Centre=OP, =3 -> Centre=SP
                                      ref int flagA    
                                      )
        {
         // ----------------------------------------------------------------------------------
            double dRadZone = 0;
            double A2 = 0;
            // ----------------------------------------------------------------------------------

            // IF1
            if (Math.Abs(dCoeffA - 1) <= 0.001)
            {
                flagA = 2; // Зона2-вокруг ОП
                A2 = 1.001;
            }
            else  // On IF1
            {
                if (dCoeffA > (1 + 0.001))
                {
                    flagA = 2; // Зона2-вокруг ОП
                    A2 = dCoeffA;
                }
                else
                {
                    flagA = 3; // Зона2-вокругSP
                    A2 = 1 / dCoeffA;
                }
            } // else1
            // ----------------------------------------------------------------------------------
            dRadZone = (dDistObj * A2) / (A2 * A2 - 1);
            // ----------------------------------------------------------------------------------
            return dRadZone;
            // ----------------------------------------------------------------------------------

        }
        // ************************************************************* R_Zone_NoSuppression

        // Delta ****************************************************************************
        // Функция определения дельта (для зоны подавления радиолиний управления)

        public double DefineDelta(
                                 // Distance SP-OP
                                 double dDistObj, 
                                 double dCoeffA
                                 )
        {
            // ----------------------------------------------------------------------------------
            double dDelta = 0;
            double A1 = 0;
            // ----------------------------------------------------------------------------------

            // IF1
            if (Math.Abs(dCoeffA - 1) <= 0.001)
            {
                A1 = 1.001;
            }

            else  // On IF1
            {
                // ***
                if (dCoeffA > 1 + 0.001)
                {
                    A1 = dCoeffA;
                }
                else
                {
                    A1 = 1 / dCoeffA;
                }
            } // else1
            // ----------------------------------------------------------------------------------
            dDelta = dDistObj / (A1 * A1 - 1);
            // ----------------------------------------------------------------------------------
            return dDelta;
            // ----------------------------------------------------------------------------------

        }
        // **************************************************************************** Delta

        // Center_ZoneNoSuppression *********************************************************
        // Определение центра зоны неподавления (для зоны подавления радиолиний управления)

        //public Point DefineCentreRNS(
        public MyPoint DefineCentreRNS(
                                    //Point tpPointOwn,
                                    //Point tpPointOpponent,
                                    MyPoint tpPointOwn,       // SP
                                    MyPoint tpPointOpponent,  // OP
                                    double dDelta
                                    )
        {
            // ----------------------------------------------------------------------------------
            MyPoint tpPointCentre = new MyPoint();

            double dHypotenuse = 0;
            double dLegTriangle1 = 0;
            double dLegTriangle2 = 0;
            double dLegTriangle3 = 0;

            double dAlfaRad = 0;

            // seg2
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            // ----------------------------------------------------------------------------------
            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;
            // ----------------------------------------------------------------------------------

            if (dDelta == 0)
            {
                tpPointCentre.X = 0;
                tpPointCentre.Y = 0;
                return tpPointCentre;
            }
            // ----------------------------------------------------------------------------------
            tpPointCentre.X = 0;
            tpPointCentre.Y = 0;

            if (tpPointOwn.X == tpPointOpponent.X)
                tpPointOpponent.X = tpPointOpponent.X + 1;

            if (tpPointOwn.Y == tpPointOpponent.Y)
                tpPointOpponent.Y = tpPointOpponent.Y + 1;

            // seg2
            dHypotenuse = Math.Sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));
            // ----------------------------------------------------------------------------------
            // если попала точка в первую четверть

            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;

                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);

                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);

            }
            // ----------------------------------------------------------------------------------
            // если попала точка во вторую четверть

            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);

                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);

            }
            // ----------------------------------------------------------------------------------
            // если попала точка в третюю четверть

            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);

                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);

            }
            // ----------------------------------------------------------------------------------
            // если попала точка в третюю четверть

            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);

                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);

            }

            return tpPointCentre;

        }
        // ********************************************************* Center_ZoneNoSuppression

        // Зона подавления радиолиний управления ********************************************

        public List<MyPoint> f_ZoneSuppression(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              RasterMapControl objRasterMapControl,

                                              // Координаты СП
                                              MyPoint tpPointOwn,
                                              // Координаты ОП
                                              MyPoint tpPointOpponent,

                                              // Hantsp
                                              int iHeightOwnAnten,
                                              // Hrelsp
                                              int iHeightOwnObj,
                                              // Hsredn
                                              int iHeightMiddle,
                                              // Hantop
                                              int iHeightOpponentAnten,
                                              // Hantop_prm
                                              int iHeightOpponentAntenPrm,

                                              // P, Wt, SP
                                              double PowerOwn,
                                              // P, Wt, OP
                                              double PowerOpponent,

                                              // К-т усиления, СП
                                              double CoeffOwn,
                                              // =1 пока подаем
                                              double Gamma,
                                              // К-т усиления, ОП
                                              double CoeffOpponent,
                                              // К-т подавления
                                              double Kp,

                                              // шаг изменения угла в град при расчете полигона
                                              int iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              int iStepLengthInput,

                                              // R zone suppression
                                              ref int R_Zone_Sup,
                                              // R zone NOsuppression
                                              ref int R_Zone_NoSup,
                                              // =2 -> Centre=OP, =3 -> Centre=SP
                                              ref int flagA,
                                              // Координаты центра зоны неподавления
                                              ref MyPoint tpPointCentreNoSup
                                              )

        {

            // ----------------------------------------------------------------------------------
            double dDistanceObject = 0;
            double dRadiusZoneSup = 0;
            double dCoeffA = 0;
            double dRadiusZoneNoSup = 0;
            double dDelta = 0;
            // ----------------------------------------------------------------------------------
            // Расстояние между СП и объектом

            dDistanceObject = DefineDistanceObject(
                                                tpPointOwn,
                                                tpPointOpponent
                                                 );
            // ----------------------------------------------------------------------------------
            // Вычисление радиуса зоны подавления

            dRadiusZoneSup = Define_R_Zone_Suppression(
                                                    // Hrelsp
                                                    iHeightOwnObj,
                                                    // Hantsp
                                                    iHeightOwnAnten,
                                                    //int iHeightPlaneOpponent, 
                                                    //Hantop_prm
                                                    iHeightOpponentAntenPrm,
                                                    // Hsredn
                                                    iHeightMiddle
                                                   );

            R_Zone_Sup = (int)dRadiusZoneSup;
            // ----------------------------------------------------------------------------------
            // Расчет коэффициента А

            dCoeffA = DefineCoeffA(
                                       // Мощность,Вт,СП
                                       PowerOwn,
                                       // К-т усиления, СП
                                       CoeffOwn,
                                       // =1 пока подаем
                                       Gamma,
                                       // Мощность,Вт,ОП
                                       PowerOpponent,
                                       // К-т усиления, ОП
                                       CoeffOpponent,
                                       // К-т подавления
                                       Kp,
                                       // Hantsp
                                       (double)iHeightOwnAnten,
                                       // Hantop
                                       (double)iHeightOpponentAnten,
                                       // Hantop_prm
                                       (double)iHeightOpponentAntenPrm
                                       );
            // ----------------------------------------------------------------------------------
            // Расчет радиуса зоны неподавления

            dRadiusZoneNoSup = DefineZoneNotSup(
                                              dDistanceObject,
                                              //ref double dCoeffA
                                              dCoeffA,
                                              // =2 -> Centre=OP, =3 -> Centre=SP
                                              ref flagA
                                              );

            R_Zone_NoSup = (int)dRadiusZoneNoSup;
            // ----------------------------------------------------------------------------------
            // Вычмсление Delta

            dDelta = DefineDelta(
                               // Distance SP-OP
                               dDistanceObject,
                               dCoeffA
                              );
            // ----------------------------------------------------------------------------------
            // Определение координат центра зоны НЕподавления

            tpPointCentreNoSup.X = 0;
            tpPointCentreNoSup.Y = 0;

            // Зона неподавления - вокруг ОП
            if (flagA == 2)
            {
                //GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup, dDelta1);
                tpPointCentreNoSup = DefineCentreRNS(
                                                  tpPointOwn,       // SP
                                                  tpPointOpponent,  // OP -> зона неподавления
                                                  dDelta
                                                    );
            }

            // Зона неподавления - вокруг SP
            else if (flagA == 3)
            {
                //GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpPoint1Rect_sup, GlobalVarLn.tpOwnCoordRect_sup, dDelta1);
                tpPointCentreNoSup = DefineCentreRNS(
                                                  tpPointOpponent,  // OP
                                                  tpPointOwn,       // SP -> зона неподавления
                                                  dDelta
                                                    );

            }
            // ----------------------------------------------------------------------------------
            // Расчет полигона зоны подавления радиолиний управления

            var pol = CreateLineSightPolygon_Circle(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              objRasterMapControl,

                                              // Координаты СП (центр зоны подавления)
                                              tpPointOwn,
                                              // HrelSP+Hantsp
                                              iHeightOwnObj+ iHeightOwnAnten,

                                              // Hantsp
                                              iHeightOwnAnten,
                                              // Hrelsp
                                              iHeightOwnObj,
                                              // Hsredn
                                              iHeightMiddle,
                                              // Hantop_prm
                                              iHeightOpponentAntenPrm,

                                              // шаг изменения угла в град при расчете полигона
                                              iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              iStepLengthInput,

                                              // R ZoneSuppression
                                              R_Zone_Sup

                                                 );
            // ----------------------------------------------------------------------------------
            return pol;
            // ----------------------------------------------------------------------------------

        }
        // ******************************************** Зона подавления радиолиний управления

        // R_ZoneSuppressionNavigation ******************************************************
        // Расчет радиуса зоны подавления навигации

        public double GetNavigationZoneRadius(
                                             // P, Wt, SP
                                             double power, 
                                             // К усиления
                                             double gain, 
                                             // К подавления
                                             double jsRatio
                                             )
        {
            // -----------------------------------------------------------------------------
            double ipwSupnav = 0;
            double dKpSupnav = 0;
            double dPSupnav = 0;
            double rr = 0;

            double VC_supnav = 300000000; // Скорость света
            double F_supnav = 1575000000;
            double iP_supnav = -155;
            // -----------------------------------------------------------------------------
            ipwSupnav = jsRatio / 10;
            dKpSupnav = Math.Pow(10, ipwSupnav);
            ipwSupnav = iP_supnav / 10;
            dPSupnav = Math.Pow(10, ipwSupnav);
            // -----------------------------------------------------------------------------
            rr = 0.7 * VC_supnav / (4 * Math.PI * F_supnav) * Math.Sqrt((power * gain) / 
                 (dKpSupnav * dPSupnav));
            // -----------------------------------------------------------------------------
            return rr;
            // -----------------------------------------------------------------------------

        }
        // ****************************************************** R_ZoneSuppressionNavigation

        // Зона подавления навигации ********************************************************

        public List<MyPoint> f_ZoneSuppressionNavigation(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              RasterMapControl objRasterMapControl,

                                              // Координаты СП
                                              MyPoint tpPointOwn,

                                              // Hantsp
                                              int iHeightOwnAnten,
                                              // Hrelsp
                                              int iHeightOwnObj,
                                              // Hsredn
                                              int iHeightMiddle,
                                              // Hantop
                                              int iHeightOpponentAnten,

                                              // P, Wt, SP
                                              double PowerOwn,
                                              // К-т усиления, СП
                                              double CoeffOwn,
                                              // К-т подавления
                                              double Kp,

                                              // шаг изменения угла в град при расчете полигона
                                              int iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              int iStepLengthInput,

                                              // R zone suppression
                                              ref int R_Zone_Sup
                                                          )

        {

            // ------------------------------------------------------------------------------
            double dRadiusZoneSup = 0;
            // ------------------------------------------------------------------------------
            // Вычисление радиуса зоны подавления навигации

            dRadiusZoneSup = GetNavigationZoneRadius(
                                              // P, Wt, SP
                                              PowerOwn,
                                              // К-т усиления, СП
                                              CoeffOwn,
                                              // К-т подавления
                                              Kp
                                                  );

            R_Zone_Sup = (int)dRadiusZoneSup;
            // ------------------------------------------------------------------------------
            // Расчет полигона зоны подавления навигации

            var pol = CreateLineSightPolygon_Circle(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              objRasterMapControl,

                                              // Координаты СП (центр зоны подавления)
                                              tpPointOwn,
                                              // HrelSP+Hantsp
                                              iHeightOwnObj + iHeightOwnAnten,

                                              // Hantsp
                                              iHeightOwnAnten,
                                              // Hrelsp
                                              iHeightOwnObj,
                                              // Hsredn
                                              iHeightMiddle,
                                              // Hantop_prm
                                              iHeightOpponentAnten,

                                              // шаг изменения угла в град при расчете полигона
                                              iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              iStepLengthInput,

                                              // R ZoneSuppression
                                              R_Zone_Sup

                                                 );
            // ----------------------------------------------------------------------------------
            return pol;
            // ----------------------------------------------------------------------------------

        }
        // ******************************************************** Зона подавления навигации

        // R_ZoneSpoofing *******************************************************************
        // Расчет радиуса зоны спуфинга

        public double GetSpoofingZoneRadius(
                                             // P, Wt, SP
                                             double power,
                                             // К усиления
                                             double gain,
                                             // К спуфинга
                                             double Kspuf
                                             )
        {
            // -----------------------------------------------------------------------------
            double dpw = 0;
            double dKspuf = 0;
            double dP = 0;
            double rr = 0;

            double VC = 300000000; // Скорость света
            double F = 1575000000;
            double P = -155;
            // -----------------------------------------------------------------------------
            dpw = Kspuf / 10;
            dKspuf = Math.Pow(10, dpw);
            dpw = P / 10;
            dP = Math.Pow(10, dpw);
            // -----------------------------------------------------------------------------
            // Зона спуфинга

            rr = 0.035 * (VC / (4 * Math.PI * F)) * Math.Sqrt((power * gain) / (dKspuf * dP));
            // -----------------------------------------------------------------------------
            return rr;
            // -----------------------------------------------------------------------------

        }
        // ******************************************************************* R_ZoneSpoofing

        // R_ZoneSpoofingJam ****************************************************************
        // Расчет радиуса зоны подавления навигации (для зоны спуфинга)

        public double GetSpoofingJamZoneRadius(
                                             // P, Wt, SP
                                             double power,
                                             // К усиления
                                             double gain,
                                             // К спуфинга
                                             double Kspuf
                                             )
        {
            // -----------------------------------------------------------------------------
            double Kspuf1 = 0;

            double dpw = 0;
            double dKspuf = 0;
            double dP = 0;
            double rr = 0;

            double VC = 300000000; // Скорость света
            double F = 1575000000;
            double P = -155;
            // -----------------------------------------------------------------------------
            Kspuf1 = Kspuf + 10;

            dpw = Kspuf1 / 10;
            dKspuf = Math.Pow(10, dpw);

            dpw = P / 10;
            dP = Math.Pow(10, dpw);
            // -----------------------------------------------------------------------------
            // Зона спуфинга

            rr = 0.035 * (VC / (4 * Math.PI * F)) * Math.Sqrt((power * gain) / (dKspuf * dP));
            // -----------------------------------------------------------------------------
            return rr;
            // -----------------------------------------------------------------------------

        }
        // **************************************************************** R_ZoneSpoofingJam

        // Зона спуфинга ********************************************************************
        // Зона спуфинга+зона подавления навигации (для зоны спуфинга)

        //public List<MyPoint> f_ZoneSpoofingJam(
        public void f_ZoneSpoofingJam(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              RasterMapControl objRasterMapControl,

                                              // Координаты СП
                                              MyPoint tpPointOwn,

                                              // Hantsp
                                              int iHeightOwnAnten,
                                              // Hrelsp
                                              int iHeightOwnObj,
                                              // Hsredn
                                              int iHeightMiddle,
                                              // Hantop
                                              int iHeightOpponentAnten,

                                              // P, Wt, SP
                                              double PowerOwn,
                                              // К-т усиления, СП
                                              double CoeffOwn,
                                              // К-т спуфинга
                                              double Kspuf,

                                              // шаг изменения угла в град при расчете полигона
                                              int iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              int iStepLengthInput,

                                              // R zone spoofing
                                              ref int R_Zone_Spuf,
                                              // R zone suppression navigation
                                              ref int R_Zone_Jam,

                                              ref List<MyPoint> polSpuf,
                                              ref List<MyPoint> polJam

                                                          )

        {

            // ------------------------------------------------------------------------------
            double dRadiusZoneSpuf = 0;
            double dRadiusZoneJam = 0;
            // ------------------------------------------------------------------------------
            // Вычисление радиуса зоны спуфинга

            dRadiusZoneSpuf = GetSpoofingZoneRadius(
                                              // P, Wt, SP
                                              PowerOwn,
                                              // К-т усиления, СП
                                              CoeffOwn,
                                              // К-т спуфинга
                                              Kspuf
                                                  );

            R_Zone_Spuf = (int)dRadiusZoneSpuf;
            // ------------------------------------------------------------------------------
            // Вычисление радиуса зоны подавления навигации (для зоны спуфинга)

            dRadiusZoneJam = GetSpoofingJamZoneRadius(
                                              // P, Wt, SP
                                              PowerOwn,
                                              // К-т усиления, СП
                                              CoeffOwn,
                                              // К-т спуфинга
                                              Kspuf
                                                  );

            R_Zone_Jam = (int)dRadiusZoneJam;
            // ------------------------------------------------------------------------------
            // Расчет полигона зоны спуфинга

            polSpuf = CreateLineSightPolygon_Circle(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              objRasterMapControl,

                                              // Координаты СП (центр зоны подавления)
                                              tpPointOwn,
                                              // HrelSP+Hantsp
                                              iHeightOwnObj + iHeightOwnAnten,

                                              // Hantsp
                                              iHeightOwnAnten,
                                              // Hrelsp
                                              iHeightOwnObj,
                                              // Hsredn
                                              iHeightMiddle,
                                              // Hantop_prm
                                              iHeightOpponentAnten,

                                              // шаг изменения угла в град при расчете полигона
                                              iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              iStepLengthInput,

                                              // R ZoneSpoofing
                                              R_Zone_Spuf

                                                 );
            // ------------------------------------------------------------------------------
            // Расчет полигона зоны подавления навигации (для зоны спуфинга)

            polJam = CreateLineSightPolygon_Circle(

                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                              objRasterMapControl,

                                              // Координаты СП (центр зоны подавления)
                                              tpPointOwn,
                                              // HrelSP+Hantsp
                                              iHeightOwnObj + iHeightOwnAnten,

                                              // Hantsp
                                              iHeightOwnAnten,
                                              // Hrelsp
                                              iHeightOwnObj,
                                              // Hsredn
                                              iHeightMiddle,
                                              // Hantop_prm
                                              iHeightOpponentAnten,

                                              // шаг изменения угла в град при расчете полигона
                                              iStepAngleInput,
                                              // шаг в м при расчете ЗПВ
                                              iStepLengthInput,

                                              // R ZoneJam
                                              R_Zone_Jam

                                                 );

            // ----------------------------------------------------------------------------------
            //return pol;
            // ----------------------------------------------------------------------------------

        }
        // ******************************************************************** Зона спуфинга





    } // Class
} // Namespace
